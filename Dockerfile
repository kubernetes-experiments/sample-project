FROM python:3.6.8-alpine3.9
RUN mkdir /app
COPY requirements.txt /app
WORKDIR /app
RUN pip install --trusted-host=pypi.python.org --trusted-host=pypi.org --trusted-host=files.pythonhosted.org -r requirements.txt
COPY . .
ENTRYPOINT ["python"]
CMD ["app.py"]
